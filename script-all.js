var allRecipies = [
    { image: "./img/cookies/cookie-1.jpg", name: "Shortbread with Chocolate", href: "./cookies/cookie-1.html" },
    { image: "./img/cookies/cookie-2.jpg", name: "Chocolate Chip Cookie Brownie Skillet", href: "./cookies/cookie-2.html" },
    { image: "./img/cookies/cookie-3.jpg", name: "Pan-banging Sugar Cookies", href: "./cookies/cookie-3.html" },
    { image: "./img/cookies/cookie-4.jpg", name: "Best Cheesecake Cookies", href: "./cookies/cookie-4.html" },
    { image: "./img/cookies/cookie-5.jpg", name: "Homemade Milano Cookies", href: "./cookies/cookie-5.html" },
    { image: "./img/desserts/dessert-1.jpg", name: "Brioche Doughnuts with Chocolate-Bourbon Glaze", href: "./desserts/dessert-1.html" },
    { image: "./img/desserts/dessert-2.jpg", name: "Cardamom Sugar Buns", href: "./desserts/dessert-2.html" },
    { image: "./img/desserts/dessert-3.jpg", name: "Chocolate Pudding Cakes with No-Churn Basil Ice Cream", href: "./desserts/dessert-3.html" },
    { image: "./img/desserts/dessert-4.jpg", name: "Espresso Semifreddo", href: "./desserts/dessert-4.html" },
    { image: "./img/desserts/dessert-5.jpg", name: "Chocolate Chip Cookie Ice Cream", href: "./desserts/dessert-5.html" },
    { image: "./img/muffins/muffin-1.jpg", name: "Pumpkin And Cream Cheese Muffins With Streusel", href: "./muffins/muffin-1.html" },
    { image: "./img/muffins/muffin-2.jpg", name: "Banana-Chocolate-Coffee Muffins with Whole Wheat", href: "./muffins/muffin-2.html" },
    { image: "./img/muffins/muffin-3.jpg", name: "Parsnip Cardamom Cupcakes with Maple Cream Cheese Buttercream", href: "./muffins/muffin-3.html" },
    { image: "./img/muffins/muffin-4.jpg", name: "Mary Berry Chocolate Cupcakes", href: "./muffins/muffin-4.html" },
    { image: "./img/muffins/muffin-5.jpg", name: "Double Chocolate Muffins", href: "./muffins/muffin-5.html" },
    { image: "./img/cakes/cake-1.jpg", name: "Chocolate Sheet Cake With Candied Walnuts", href: "./cakes/cake-1.html" },
    { image: "./img/cakes/cake-2.jpg", name: "Meyer Lemon Cake With Crème Fraîche Glaze", href: "./cakes/cake-2.html" },
    { image: "./img/cakes/cake-3.jpg", name: "Lovely Lemon <br> Yogurt Cake", href: "./cakes/cake-3.html" },
    { image: "./img/cakes/cake-4.jpg", name: "Chocolate Cake With Espresso Buttercream", href: "./cakes/cake-4.html" },
    { image: "./img/cakes/cake-7.jpg", name: "Strawberry Shortcake Cake, In Three Layers", href: "./cakes/cake-7.html" },
    { image: "./img/cakes/cake-6.jpg", name: "Dark Chocolate Snack Cake", href: "./cakes/cake-6.html" },
    { image: "./img/cakes/cake-5.jpg", name: "Powdered Donut Cake", href: "./cakes/cake-5.html" },
    { image: "./img/cakes/cake-8.jpg", name: "Banana Layer Cake", href: "./cakes/cake-8.html" },
    { image: "./img/cakes/cake-9.jpg", name: "Izy’s Swedish Chocolate Cake", href: "./cakes/cake-9.html" },
    { image: "./img/cakes/cake-10.jpg", name: "Vanilla Bourbon Cake", href: "./cakes/cake-10.html" },
    { image: "./img/pies/pie-1.jpg", name: "Peanut Butter Chocolate Tart", href: "./pies/pie-1.html" },
    { image: "./img/pies/pie-2.jpg", name: "Blueberry Walnut Streusel Pie", href: "./pies/pie-2.html" },
    { image: "./img/pies/pie-31.jpg", name: "Coffee Caramel Ice Cream Pie", href: "./pies/pie-3.html" },
    { image: "./img/pies/pie-4.jpg", name: "Pear Apple Hard Cider Pie", href: "./pies/pie-4.html" },
    { image: "./img/pies/pie-5.jpg", name: "Bittersweet Chocolate Shortbread Tarts with Brown Sugar Whipped Cream", href: "./pies/pie-5.html" }
]

allRecipies = allRecipies.sort(() => Math.random() - 0.5)

var recipie = '<div class="row">';

for (var i = 0; i < allRecipies.length; i++) {
    recipie += '<div class="col"">';
    recipie += '<div class="card"">';
    recipie += '<img class="card-img-top" src=' + allRecipies[i].image + ' alt="Card image cap">';
    recipie += '<div class="card-body">';
    recipie += '<h5 class="card-title">' + allRecipies[i].name + '</h5>';
    recipie += '<hr>';
    recipie += '<a href=' + allRecipies[i].href + ' class="btn btn-dark">See recipe</a>';
    recipie += '</div>';
    recipie += '</div>';
    recipie += '</div>';
}

recipie += '</div>';

document.getElementById('allRecipies').innerHTML = recipie;