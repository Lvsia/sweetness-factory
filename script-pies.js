/*----------------------- PIES -----------------------*/

var pies = [
    { image: "../img/pies/pie-1.jpg", name: "Peanut Butter Chocolate Tart", href: "../pies/pie-1.html" },
    { image: "../img/pies/pie-2.jpg", name: "Blueberry Walnut Streusel Pie", href: "../pies/pie-2.html" },
    { image: "../img/pies/pie-31.jpg", name: "Coffee Caramel Ice Cream Pie", href: "../pies/pie-3.html" },
    { image: "../img/pies/pie-4.jpg", name: "Pear Apple Hard Cider Pie", href: "../pies/pie-4.html" },
    { image: "../img/pies/pie-5.jpg", name: "Bittersweet Chocolate Shortbread Tarts with Brown Sugar Whipped Cream", href: "../pies/pie-5.html" }
];

var pie = '<div class="row">';

for (var i = 0; i < pies.length; i++) {
    pie += '<div class="col">';
    pie += '<div class="card">';
    pie += '<img class="card-img-top" src=' + pies[i].image + ' alt="Card image cap">';
    pie += '<div class="card-body">';
    pie += '<h5 class="card-title">' + pies[i].name + '</h5>';
    pie += '<hr>';
    pie += '<a href=' + pies[i].href + ' class="btn btn-dark">See recipe</a>';
    pie += '</div>';
    pie += '</div>';
    pie += '</div>';
}

pie += '</div>';

document.getElementById('piesList').innerHTML = pie;