/*----------------------- CAKES -----------------------*/

var cakes = [
    { image: "../img/cakes/cake-1.jpg", name: "Chocolate Sheet Cake With Candied Walnuts", href: "../cakes/cake-1.html" },
    { image: "../img/cakes/cake-2.jpg", name: "Meyer Lemon Cake With Crème Fraîche Glaze", href: "../cakes/cake-2.html" },
    { image: "../img/cakes/cake-3.jpg", name: "Lovely Lemon <br> Yogurt Cake", href: "../cakes/cake-3.html" },
    { image: "../img/cakes/cake-4.jpg", name: "Chocolate Cake With Espresso Buttercream", href: "../cakes/cake-4.html" },
    { image: "../img/cakes/cake-7.jpg", name: "Strawberry Shortcake Cake, In Three Layers", href: "../cakes/cake-7.html" },
    { image: "../img/cakes/cake-6.jpg", name: "Dark Chocolate Snack Cake", href: "../cakes/cake-6.html" },
    { image: "../img/cakes/cake-5.jpg", name: "Powdered Donut Cake", href: "../cakes/cake-5.html" },
    { image: "../img/cakes/cake-8.jpg", name: "Banana Layer Cake", href: "../cakes/cake-8.html" },
    { image: "../img/cakes/cake-9.jpg", name: "Izy’s Swedish Chocolate Cake", href: "../cakes/cake-9.html" },
    { image: "../img/cakes/cake-10.jpg", name: "Vanilla Bourbon Cake", href: "../cakes/cake-10.html" }
];
  
var cakey = '<div class="row">';

for (var i = 0; i < cakes.length; i++) {
    cakey += '<div class="col"">';
    cakey += '<div class="card"">';
    cakey += '<img class="card-img-top" src=' + cakes[i].image + ' alt="Card image cap">';
    cakey += '<div class="card-body">';
    cakey += '<h5 class="card-title">' + cakes[i].name + '</h5>';
    cakey += '<hr>';
    cakey += '<a href=' + cakes[i].href + ' class="btn btn-dark">See recipe</a>';
    cakey += '</div>';
    cakey += '</div>';
    cakey += '</div>';
}

cakey += '</div>';

document.getElementById('cakesList').innerHTML = cakey;