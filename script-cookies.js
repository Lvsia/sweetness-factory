/*----------------------- COOKIES -----------------------*/

var cookies = [
    { image: "../img/cookies/cookie-1.jpg", name: "Shortbread with Chocolate", href: "../cookies/cookie-1.html" },
    { image: "../img/cookies/cookie-2.jpg", name: "Chocolate Chip Cookie Brownie Skillet", href: "../cookies/cookie-2.html" },
    { image: "../img/cookies/cookie-3.jpg", name: "Pan-banging Sugar Cookies", href: "../cookies/cookie-3.html" },
    { image: "../img/cookies/cookie-4.jpg", name: "Best Cheesecake Cookies", href: "../cookies/cookie-4.html" },
    { image: "../img/cookies/cookie-5.jpg", name: "Homemade Milano Cookies", href: "../cookies/cookie-5.html" }
];

var cookie = '<div class="row">';

for (var i = 0; i < cookies.length; i++) {
    cookie += '<div class="col"">';
    cookie += '<div class="card"">';
    cookie += '<img class="card-img-top" src=' + cookies[i].image + ' alt="Card image cap">';
    cookie += '<div class="card-body">';
    cookie += '<h5 class="card-title">' + cookies[i].name + '</h5>';
    cookie += '<hr>';
    cookie += '<a href=' + cookies[i].href + ' class="btn btn-dark">See recipe</a>';
    cookie += '</div>';
    cookie += '</div>';
    cookie += '</div>';
}

cookie += '</div>';

document.getElementById('cookiesList').innerHTML = cookie;