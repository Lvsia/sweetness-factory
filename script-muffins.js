/*----------------------- MUFFINS -----------------------*/

var muffins = [
    { image: "../img/muffins/muffin-1.jpg", name: "Pumpkin And Cream Cheese Muffins With Streusel", href: "../muffins/muffin-1.html" },
    { image: "../img/muffins/muffin-2.jpg", name: "Banana-Chocolate-Coffee Muffins with Whole Wheat", href: "../muffins/muffin-2.html" },
    { image: "../img/muffins/muffin-3.jpg", name: "Parsnip Cardamom Cupcakes with Maple Cream Cheese Buttercream", href: "../muffins/muffin-3.html" },
    { image: "../img/muffins/muffin-4.jpg", name: "Mary Berry Chocolate Cupcakes", href: "../muffins/muffin-4.html" },
    { image: "../img/muffins/muffin-5.jpg", name: "Double Chocolate Muffins", href: "../muffins/muffin-5.html" }
];

var muffin = '<div class="row">';

for (var i = 0; i < muffins.length; i++) {
    muffin += '<div class="col">';
    muffin += '<div class="card">';
    muffin += '<img class="card-img-top" src=' + muffins[i].image + ' alt="Card image cap">';
    muffin += '<div class="card-body">';
    muffin += '<h5 class="card-title">' + muffins[i].name + '</h5>';
    muffin += '<hr>';
    muffin += '<a href=' + muffins[i].href + ' class="btn btn-dark">See recipe</a>';
    muffin += '</div>';
    muffin += '</div>';
    muffin += '</div>';
}

muffin += '</div>';

document.getElementById('muffinsList').innerHTML = muffin;