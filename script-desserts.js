/*----------------------- DESSERTS -----------------------*/

var desserts = [
    { image: "../img/desserts/dessert-1.jpg", name: "Brioche Doughnuts with Chocolate-Bourbon Glaze", href: "../desserts/dessert-1.html" },
    { image: "../img/desserts/dessert-2.jpg", name: "Cardamom Sugar Buns", href: "../desserts/dessert-2.html" },
    { image: "../img/desserts/dessert-3.jpg", name: "Chocolate Pudding Cakes with No-Churn Basil Ice Cream", href: "../desserts/dessert-3.html" },
    { image: "../img/desserts/dessert-4.jpg", name: "Espresso Semifreddo", href: "../desserts/dessert-4.html" },
    { image: "../img/desserts/dessert-5.jpg", name: "Chocolate Chip Cookie Ice Cream", href: "../desserts/dessert-5.html" }
];

var dessert = '<div class="row">';

for (var i = 0; i < desserts.length; i++) {
    dessert += '<div class="col">';
    dessert += '<div class="card">';
    dessert += '<img class="card-img-top" src=' + desserts[i].image + ' alt="Card image cap">';
    dessert += '<div class="card-body">';
    dessert += '<h5 class="card-title">' + desserts[i].name + '</h5>';
    dessert += '<hr>';
    dessert += '<a href=' + desserts[i].href + ' class="btn btn-dark">See recipe</a>';
    dessert += '</div>';
    dessert += '</div>';
    dessert += '</div>';
}

dessert += '</div>';

document.getElementById('dessertsList').innerHTML = dessert;